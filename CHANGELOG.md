# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2020-08-29
### Added
- Support for mousover tooltips over the roll formula

## [1.1.0] - 2020-07-16
Initial public release

### Features
Roll template for use with the [GURPS Foundry Roll Library](https://gitlab.com/gurps-foundry/roll-lib)  