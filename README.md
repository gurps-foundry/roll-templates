# GURPS Foundry Roll Templates

This Foundry VTT module contains the default html templates used by the [GURPS Foundry Roll Library](https://gitlab.com/gurps-foundry/roll-lib). Unless you wish to use your own html templates with the roll library, you will need to include this module in your worlds.